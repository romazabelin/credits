# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Creating solution for test task

### Requirements ###
PHP version 7.3

### How do I get set up? ###

1)Clone repository
git clone https://romazabelin@bitbucket.org/romazabelin/credits.git

2)Setup localhost. Example for WAMP:
- File D:\wamp64_3.1.9\bin\apache\apache2.4.39\conf\extra\httpd-vhosts.conf
<VirtualHost *:80>
    DocumentRoot "D:/wamp64_3.1.9/www/credits/public"
    ServerName credits
    ServerAlias credits
    <Directory "D:/wamp64_3.1.9/www/credits/public">
        Order allow,deny
        Allow from all
    </Directory>
</VirtualHost>

- File C:\Windows\System32\drivers\etc\hosts:
127.0.0.1 credits

3)Run 'Composer install'

4)Setup DB credits in .env
DATABASE_URL=mysql://{user}:{pass}@127.0.0.1:3306/{dbName}?serverVersion=5.7

4)Create database with
php bin/console doctrine:database:create

5)Run migrations
php bin/console doctrine:migrations:migrate

Check it!

