<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200630072341 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE loan (id INT AUTO_INCREMENT NOT NULL, start_date DATETIME NOT NULL, total BIGINT NOT NULL, term INT NOT NULL, percent_rate DOUBLE PRECISION DEFAULT NULL, created_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE payment (id INT AUTO_INCREMENT NOT NULL, loan_id INT DEFAULT NULL, payment_date DATETIME NOT NULL, sum DOUBLE PRECISION NOT NULL, sum_percent DOUBLE PRECISION NOT NULL, sum_loan DOUBLE PRECISION NOT NULL, balance DOUBLE PRECISION NOT NULL, INDEX IDX_6D28840DCE73868F (loan_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE payment ADD CONSTRAINT FK_6D28840DCE73868F FOREIGN KEY (loan_id) REFERENCES loan (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE payment DROP FOREIGN KEY FK_6D28840DCE73868F');
        $this->addSql('DROP TABLE loan');
        $this->addSql('DROP TABLE payment');
    }
}
