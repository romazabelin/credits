<?php


namespace App\Service;

use App\Entity\Loan;
use App\Entity\Payment;
use App\Service\Interfaces\PaymentInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ManageLoan
{
    private $container;

    private $payment;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * store loan and calculate all payments for it
     *
     * @param PaymentInterface $payment
     */
    public function storeLoan(PaymentInterface $payment)
    {
        $this->payment = $payment;
        $this->payment->calculatePayments();
    }
}