<?php
/**
 * Created by PhpStorm.
 * User: roma
 * Date: 7/1/20
 * Time: 8:40 AM
 */

namespace App\Service;

use App\Entity\Loan;
use App\Entity\Payment;
use App\Service\Interfaces\PaymentInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class MonoBank implements PaymentInterface
{
    protected $container;
    protected $startDate;
    protected $total;
    protected $term;
    protected $percentRate;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param string $startDate
     * @param int $total
     * @param int $term
     * @param float $percentRate
     */
    public function setData(\DateTime $startDate, int $total, int $term, float $percentRate)
    {
        $this->startDate = $startDate;
        $this->total     = $total;
        $this->term      = $term;
        $this->percentRate = $percentRate;
    }

    /**
     * calculate payments for loan
     */
    public function calculatePayments()
    {
        $em = $this->container->get('doctrine')->getManager();

//        if (!$this->startDate)
//            $this->startDate = date('Y/m/d');

//        $d    = str_replace("-", "/", $this->startDate);
//        $date = \DateTime::createFromFormat('Y/m/d', $d);
//        $date->setTime(0, 0, 0);

        $totalPercentPerYear  = round(($this->percentRate * $this->total) / 100, 2);
        $totalPercentPerMonth = round(($totalPercentPerYear) / 12, 2);
        $totalLoanPerMonth    = round($this->total / $this->term, 2);
        $paymentPerMonth      = $totalLoanPerMonth + $totalPercentPerMonth;

        //create loan
        $loan = new Loan();

        $loan->setTotal((int)$this->total);
        $loan->setTerm((int)$this->term);
        $loan->setPercentRate((float)$this->percentRate);
        $loan->setStartDate($this->startDate);

        $em->persist($loan);

        //calculate all payments for specific term
        for($i = 0; $i < $this->term; $i++) {
            $addMonthDate = clone $this->startDate;
            $addMonthDate->modify('+' . ($i + 1) . ' months');

            $payment = new Payment();
            $payment->setSumPercent($totalPercentPerMonth);
            $payment->setSumLoan($totalLoanPerMonth);
            $payment->setBalance($paymentPerMonth * ($this->term - $i));
            $payment->setLoan($loan);
            $payment->setPaymentDate($addMonthDate);

            $em->persist($payment);
        }

        $em->flush();
    }
}