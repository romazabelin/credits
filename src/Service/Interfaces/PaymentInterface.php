<?php


namespace App\Service\Interfaces;

interface PaymentInterface
{
    public function calculatePayments();
}

