<?php


namespace App\Controller;

use App\Entity\Loan;
use App\Entity\Payment;
use App\Form\LoanForm;
use App\Service\ManageLoan;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;

class LoanController extends AbstractController
{
    /**
     * @Route("/loan-show/{id}", name="loan_show")
     */
    public function loanShow($id)
    {
        $em    = $this->getDoctrine()->getManager();
        $payments = $em->getRepository(Payment::class)->findBy(['loan' => $id]);

        return $this->render('loan/show.html.twig', [
            'payments' => $payments
        ]);
    }

    /**
     * @Route("/loan-list", name="loan_list")
     */
    public function loanList()
    {
        $em    = $this->getDoctrine()->getManager();
        $loans = $em->getRepository(Loan::class)->findAll();

        return $this->render('loan/index.html.twig', [
            'loans' => $loans
        ]);
    }

    /**
     * @Route("/loan-add", name="loan_add")
     */
    public function loanAdd(Request $request, ManageLoan $loanService, \App\Service\MonoBank $monoBank)
    {
        $em   = $this->getDoctrine()->getManager();
        $form = $this->createForm(LoanForm::class, new Loan());

        try {
            $form->handleRequest($request);
        } catch(\Exception $e) {
            echo $e->getMessage();
        } catch(\Throwable $e) {
            echo $e->getMessage();
        }

        if ($form->isSubmitted() && $form->isValid()) {
            $data        = $form->getData();
            $startDate   = $data->getStartDate();
            $total       = $data->getTotal();
            $term        = $data->getTerm();
            $percentRate = $data->getPercentRate();

            try {
                //set data
                $monoBank->setData($startDate , $total, $term, $percentRate);

                //calculate data for specific bank
                $loanService->storeLoan($monoBank);

                return $this->redirectToRoute('loan_list');
            } catch(\Exception $e) {
                echo $e->getMessage();
            } catch(\Throwable $e) {
                echo $e->getMessage();
            }
        }

        return $this->render('loan/add.html.twig', [
            'form' => $form->createView()
        ]);
    }
}