<?php


namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class LoanForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('startDate', DateType::class, [
                'label' => 'loan.start_date',
            ])
            ->add('total', TextType::class, [
                'label' => 'loan.loan_sum',
            ])
            ->add('term', TextType::class, [
                'label' => 'loan.loan_term',
            ])
            ->add('percentRate', TextType::class, [
                'label' => 'loan.annual_percent',
            ])
            ->add('save', SubmitType::class, [
                'label' => 'loan.save',
            ]);
    }
}